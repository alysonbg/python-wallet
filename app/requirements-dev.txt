-r requirements.txt
flake8==4.0.1
pytest-django==4.4.0
pytest-cov==2.12.1
pytest==6.2.4
