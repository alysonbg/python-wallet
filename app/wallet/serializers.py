from rest_framework import serializers
from wallet.models import Customer, Order, OrderLine
from wallet.bussiness_logic import calculate_cashback


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ("document", "name")

    def validate_document(self, value):
        """
        Check that document has 11 digits and contains only numbers.
        """
        if len(value) != 11:
            raise serializers.ValidationError("Document must have 11 digits!")
        if not value.isdigit():
            raise serializers.ValidationError("Document must contain only numbers!")

        return value


class OrderLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = ("type", "value", "qty")


class OrderSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()
    products = OrderLineSerializer(many=True, source="orderlines")

    class Meta:
        model = Order
        fields = ("sold_at", "total", "cashback", "customer", "products")

    def validate(self, data):
        """
        Check if the total is correct.
        """
        products = data["orderlines"]
        amount = data["total"]
        total = 0
        for product in products:
            total += product.get("value") * product.get("qty")

        if amount != total:
            raise serializers.ValidationError("Total is incorrect!")

        return data

    def create(self, validated_data):
        customer = Customer.objects.get_or_create(
            document=validated_data["customer"]["document"],
            name=validated_data["customer"]["name"]
        )
        cashback = calculate_cashback(validated_data["total"])
        order = Order.objects.create(
            sold_at=validated_data["sold_at"],
            total=validated_data["total"],
            customer=customer[0],
            cashback=cashback
        )

        for line in validated_data["orderlines"]:
            OrderLine.objects.create(
                order=order,
                type=line["type"],
                value=line["value"],
                qty=line["qty"],
            )
        return order
