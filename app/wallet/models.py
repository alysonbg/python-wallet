from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    pass


class Customer(models.Model):
    document = models.CharField("Document", max_length=11, unique=True)
    name = models.CharField("Name", max_length=50)

    def __str__(self):
        return f"{self.name}"


class Order(models.Model):
    sold_at = models.DateTimeField("Solt at")
    total = models.DecimalField("Total", max_digits=6, decimal_places=2, null=True, blank=True)
    cashback = models.DecimalField("Cashback", max_digits=6, decimal_places=2, null=True, blank=True)
    customer = models.ForeignKey("Customer", null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f"{self.customer.name} {self.sold_at}"


class OrderLine(models.Model):
    type = models.CharField("Type", max_length=10)
    value = models.DecimalField("Value", max_digits=6, decimal_places=2, null=True, blank=True)
    qty = models.IntegerField("Qty")
    order = models.ForeignKey("Order", on_delete=models.CASCADE, related_name='orderlines')
