from django.urls import path
from .views import OrderCreate


urlpatterns = [
    path('cashback/', OrderCreate.as_view()),
]
