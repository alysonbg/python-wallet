import pytest


@pytest.mark.django_db
def test_add_order(client):
    resp = client.post(
        "/api/cashback/",
        {
            "sold_at": "2026-01-02 00:00:00",
            "customer": {
                "document": "12345678901",
                "name": "John Wick"
            },
            "total": "100.00",
            "products": [
                {
                    "type": "A",
                    "value": "10.00",
                    "qty": 1,
                },
                {
                    "type": "B",
                    "value": "10.00",
                    "qty": 9,
                }
            ],
        },
        content_type="application/json"
    )
    assert resp.status_code == 201
