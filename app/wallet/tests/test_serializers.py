import pytest
from rest_framework import serializers
from wallet.models import Customer, Order
from wallet.serializers import CustomerSerializer, OrderLineSerializer, OrderSerializer


@pytest.fixture
def order(db):
    customer = Customer.objects.create(
        document="12345678901",
        name="Neo"
    )
    return Order.objects.create(
        sold_at="2026-01-02 00:00:00",
        customer=customer
    )


@pytest.mark.django_db
def test_customer_serializer_date():
    data = dict(
        document="12345678901",
        name="Neo"
    )

    serializer = CustomerSerializer(data=data)

    serializer.is_valid()

    assert {"document": "12345678901", "name": "Neo"} == serializer.data


@pytest.mark.django_db
def test_customer_serializer_validate_document_length():
    data = dict(
        document="123456789011231",
        name="Neo"
    )

    serializer = CustomerSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_customer_serializer_validate_special_characters():
    data = dict(
        document="1234567890@",
        name="Neo"
    )

    serializer = CustomerSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_order_line_serializer_data(order):
    data = dict(
        type="A",
        value="100.00",
        qty=2,
        order=order
    )
    serializer = OrderLineSerializer(data=data)
    serializer.is_valid()
    assert {
        "type": "A",
        "value": "100.00",
        "qty": 2,
    } == serializer.data


@pytest.mark.django_db
def test_order_serializer_data():
    data = dict(
        sold_at="2026-01-02 00:00:00",
        customer={
            "document": "12345678901",
            "name": "John Wick"
        },
        total="100.00",
        products=[
            {
                "type": "A",
                "value": "10.00",
                "qty": 1,
            },
            {
                "type": "B",
                "value": "10.00",
                "qty": 9,
            }
        ]
    )
    serializer = OrderSerializer(data=data)
    serializer.is_valid()

    assert {
        "sold_at": "2026-01-02 00:00:00",
        "customer": {
            "document": "12345678901",
            "name": "John Wick"
        },
        "total": "100.00",
        "products": [
            {
                "type": "A",
                "value": "10.00",
                "qty": 1,
            },
            {
                "type": "B",
                "value": "10.00",
                "qty": 9,
            }
        ],
        "cashback": None
    } == serializer.data


@pytest.mark.django_db
def test_order_serializer_total_validation():
    data = dict(
        sold_at="2026-01-02 00:00:00",
        customer={
            "document": "12345678901",
            "name": "John Wick"
        },
        total="90.00",
        products=[
            {
                "type": "A",
                "value": "10.00",
                "qty": 1,
            },
            {
                "type": "B",
                "value": "10.00",
                "qty": 9,
            }
        ]
    )
    serializer = OrderSerializer(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)
