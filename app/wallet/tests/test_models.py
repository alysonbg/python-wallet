from datetime import datetime
import pytest
from wallet.models import Customer, Order, OrderLine


def test_create_customer(db):
    assert Customer.objects.create(
        document="12345678901",
        name="Neo"
    )


def test_customer_str(db):
    customer = Customer.objects.create(
        document="12345678901",
        name="Neo"
    )
    assert "Neo" == str(customer)


@pytest.fixture
def customer(db):
    return Customer.objects.create(
        document="12345678901",
        name="Neo"
    )


def test_create_order(db, customer):
    assert Order.objects.create(
        sold_at=datetime(2021, 8, 10, 15, 30, 0),
        customer=customer,
    )


def test_order_str(db, customer):
    order = Order.objects.create(
        sold_at=datetime(2021, 8, 10, 15, 30, 0),
        customer=customer,
    )
    assert "Neo 2021-08-10 15:30:00" == str(order)


@pytest.fixture
def order(db, customer):
    return Order.objects.create(
        sold_at=datetime(2021, 8, 10, 15, 30, 0),
        customer=customer,
    )


def test_create_order_line(db, order):
    assert OrderLine.objects.create(
        type="A",
        value=100.00,
        qty=5,
        order=order
    )
