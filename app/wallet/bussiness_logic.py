from decimal import Decimal


def calculate_cashback(value):
    """Function that calculates the total of cashback for a sale"""
    return Decimal(Decimal(value) * Decimal(0.1))
